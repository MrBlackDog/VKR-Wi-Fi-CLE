import datetime
import json


class Config():

    def __init__(self):

        self.isworking = 1

        # COORDINATES CALCULATING PARAMS
        filename = "logs/" + str(datetime.datetime.now()) + "_" + "ALL.txt"
        filename = filename.replace(" ", "_")
        filename = filename.replace(":", "_")
        filename = filename.replace("-", "_")
        self.filename = filename

        self.f = None
        # list of anchors to configure
        self.anchors_conf = []
        with open("anchors.json", "r") as file:
            for line in file:
                self.anchors_conf.append(json.loads(line))