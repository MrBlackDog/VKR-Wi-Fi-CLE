# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import asyncio
import aiofiles as aiof
# Press Ctrl+F8 to toggle the breakpoint.
import json

import aiohttp as aiohttp

from config import Config


async def AnchorRequesting(config, i, queue):
    while True:
        async with aiohttp.ClientSession() as session:
            async with session.get('http://' + config.anchors_conf[i]["IP"] + '/getlog') as response:
                # async with session.get('http://python.org') as response:
                # async with session.get("http://192.168.1.12/getlog") as response:
                # print("Client ", i, " status:", response.status)
                # print("Content-type:", response.headers['content-type'])
                # html = await response.text()
                assert response.status == 200
                html = await response.text()
                # print("Body:", i, ' ' + html)
                await queue.put(str(config.anchors_conf[i]["Coordinate"]) + " " + html)
                await asyncio.sleep(3)
    # reader, writer = await asyncio.open_connection(anchors_conf[i]["IP"], anchors_conf[i]['Port'])
    # # config request from current anchor
    # data = await reader.read(3)
    # data = await reader.read(500)
    # while True:
    #     header = await reader.read(3)
    #     numberofbytes = header[1]
    #     data = await reader.read(numberofbytes)
    #     ending = await reader.read(3)

    # mes = rm.Message(data)
    # mes.Anchor = AnchorID
    # if mes.type == "CS_TX":
    #     mes.tx_ID = AnchorID
    #
    #     await queue.put(data)
    #
    #  s.sendall(rm.build_RTLS_START_REQ(0))
    # print(AnchorID + " stopped")


# функция процесса, который расчищает буфер и кидает все данные в обработку
async def MainProcessing(config, queue):
    async with aiof.open(config.filename, 'w') as f:
        for anchor in config.anchors_conf:
            await f.write(anchor["Coordinate"] + "\n")
            # await f.write(str(i))
            await f.flush()
        await f.write("measurements started" + "\n")
        await f.flush()
        print('measurements started')
        while True:
            # print(123)
            mes = await queue.get()
            print("MAIN: " + mes)
            await f.write(mes + "\n")
            await f.flush()


# IT WORKS
# async def main():
#
#     async with aiohttp.ClientSession() as session:
#         async with session.get('http://192.168.1.12/getlog') as response:
#
#             print("Status:", response.status)
#             print("Content-type:", response.headers['content-type'])
#
#             html = await response.text()
#             print("Body:", html)
# async def fetch(client):
#     async with client.get('http://python.org') as resp:
#         assert resp.status == 200
#         return await resp.text()
#
#
# async def main():
#     async with aiohttp.ClientSession() as client:
#         html = await fetch(client)
#         print(html)


if __name__ == '__main__':

    # IT WORKS
    # loop = asyncio.get_event_loop()
    # loop.run_until_complete(main())

    # load settings
    config = Config()
    loop = asyncio.get_event_loop()
    queue = asyncio.Queue()
    tasks = []
    tasks.append(loop.create_task(MainProcessing(config, queue)))
    for i in range(len(config.anchors_conf)):
        tasks.append(loop.create_task(AnchorRequesting(config, i, queue)))
    loop.run_until_complete(asyncio.wait(tasks))
    loop.close()